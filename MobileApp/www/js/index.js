var app = {

  initialize: function() {
    // document.addEventListener('deviceready', this.onDeviceReady.bind(this), false);

    var handleClickFromParent = function() {
      var obj = {"result": "fromParent"};
      var childWindow = document.getElementById('myFrame').contentWindow;
      childWindow.postMessage(JSON.stringify(obj), '*');

      document.getElementById('pValue').value = 'clicked!';
    };
    document.getElementById('myBtnFrame').addEventListener('click', handleClickFromParent, false);


    window.addEventListener('message', function(e) {
      document.getElementById('pValue').value = JSON.parse(e.data).result;
    }, false);

  },

  // onDeviceReady: function() {
  //     window.addEventListener('message', function(e) {
  //         // var childWindow = document.getElementById('myFrame').contentWindow;
  //         // if (e.source !== childWindow) {
  //         //     document.getElementById('pValue').value = 'clicked!';
  //         //     return;
  //         // }
  //         document.getElementById('pValue').value = JSON.parse(e.data).result;
  //     }, false);
  // }

};

app.initialize();