function postMsg() {
  document.getElementById('myBtn').addEventListener('click', handleClick, false);
}

function handleClick() {
  //var myEvent = new CustomEvent('iframe_message', {detail: 'iframeValue'});
  // window.parent.document.dispatchEvent(myEvent);

  var obj = {"msg": "fromChild", "result": "fromIFrame"};
  window.parent.postMessage(JSON.stringify(obj), '*');
  document.getElementById('cValue').value = 'clicked!';
}

window.addEventListener('message', function(e) {
  document.getElementById('cValue').value = JSON.parse(e.data).result;
}, false);

